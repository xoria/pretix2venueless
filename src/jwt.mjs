import JWT from "jsonwebtoken";

export async function sign(payload) {
  return await new Promise((resolve, reject) => {
    const _now = (Date.now() / 1000) | 0;
    JWT.sign(
      {
        iss: process.env.JWT_ISS,
        aud: process.env.JWT_AUD,
        exp: _now + 3600 * 24 * 14,
        iat: _now,
        ...payload,
      },
      process.env.JWT_SECRET,
      { algorithm: "HS256" },
      (err, token) => {
        if (err != null) {
          return reject(err);
        }
        resolve(token);
      }
    );
  });
}
