import path from "node:path";
import { fileURLToPath } from "node:url";
import { createTransport } from "nodemailer";
import _ from "lodash";
import { readJSON } from "./fs.mjs";
import { readFile } from "node:fs/promises";
import { DRY_SMTP } from "./dry.service.mjs";

/** @typedef { import('lodash').TemplateExecutor } TemplateExecutor */
/** @typedef { import('./types').EmailData } EmailData */

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @type {Record<string, TemplateExecutor>}
 */
const executors = {};

export const INDEX = await readJSON(
  path.join(__dirname, "../email-templates/index.json")
);

export const ITEM_IDS = INDEX.flatMap((it) => it.item);

export const INDEX_ID_BY_ITEM = Object.fromEntries(
  INDEX.flatMap(({ item }, id) =>
    (Array.isArray(item) ? item : [item]).map((item) => [item, id])
  )
);

const transporter = createTransport({
  host: process.env.SMTP_HOST,
  port: +process.env.SMTP_PORT,
  secure: false, // use STARTTLS
  auth: {
    user: process.env.SMTP_USER,
    pass: process.env.SMTP_PASSWORD,
  },
});

/**
 * @param {EmailData} emailData
 * @returns {Promise<void>}
 */
export async function sendMailByTemplate(emailData) {
  await Promise.all(
    Object.entries(
      _.groupBy(emailData.tickets, (it) => INDEX_ID_BY_ITEM[it.item])
    ).map(async ([indexId, tickets]) => {
      const {
        template: { subject, text, html },
      } = INDEX[indexId];
      const [subjectTpl, textTpl, htmlTpl] = await Promise.all([
        parseTemplate(subject),
        parseTemplate(text),
        parseTemplate(html),
      ]);
      const payload = { email: emailData.email, tickets };
      const subjectBody = subjectTpl?.(payload);
      const textBody = textTpl?.(payload);
      const htmlBody = htmlTpl?.(payload);
      await sendMail({
        from: process.env.EMAIL_FROM,
        subject: subjectBody,
        to: emailData.email,
        text: textBody,
        html: htmlBody,
      });
    })
  );
}

/**
 * @param {string=} filename
 * @returns {Promise<TemplateExecutor>}
 */
async function parseTemplate(filename) {
  if (!filename) {
    return undefined;
  }
  if (Reflect.has(executors, filename)) {
    return await executors[filename];
  }
  return await (executors[filename] = readTemplate(filename));
}

async function readTemplate(filenameOrTemplate) {
  let body;
  try {
    body = await readFile(
      path.resolve(__dirname, "../email-templates", filenameOrTemplate)
    );
  } catch (ignored) {
    body = filenameOrTemplate;
  }
  return _.template(body.toString());
}

async function sendMail({ from, to, subject, text, html }) {
  if (!DRY_SMTP) {
    await transporter.sendMail({ from, to, subject, text, html });
  }
  console.debug(`${DRY_SMTP ? "[DRY] " : ""}email sent to`, to);
}
