import { fetchJSON } from "./http.mjs";
import { ITEM_IDS } from "./email.service.mjs";
import { STATE } from "./state.service.mjs";
import { readJSON } from "./fs.mjs";
import { fileURLToPath } from "node:url";
import path from "node:path";
import { DRY_PRETIX } from "./dry.service.mjs";

/** @typedef { import('./types').Ticket } Ticket */
/** @typedef { import('./types').OrderPositionDTO } OrderPositionDTO */

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

/**
 * @returns {Promise<Ticket[]>}
 */
export async function fetchTickets() {
  const orders = await fetchOrders();
  return orders.flatMap((order) => {
    const datetime = new Date(order.datetime);
    return order.positions
      .filter(isValidTicket)
      .filter((it) => !STATE.processedIds.has(it.id))
      .map((position) => ({
        datetime,
        id: position.id,
        item: position.item,
        email: position.attendee_email ?? order.email,
        answers: positionToAnswers(position),
      }));
  });
}

/**
 * @returns {Promise<OrderDTO[]>}
 */
async function fetchOrders() {
  let items = [];
  const { latestDate } = STATE;
  const query = latestDate ? `?created_since=${latestDate.toISOString()}` : "";
  let next = `${process.env.PRETIX_URL}v1/organizers/${process.env.PRETIX_ORGANIZER}/events/${process.env.PRETIX_EVENT}/orders/${query}`;
  while (next != null) {
    let res;
    if (DRY_PRETIX) {
      try {
        let results = await readJSON(path.join(__dirname, "pretix.mock.json"));
        res = { count: results.length, next: null, results };
      } catch (ignored) {
        console.debug("pretix.mock.json cannot be read. Ignored.");
        res = { count: 0, next: null, results: [] };
      }
    } else {
      res = await fetchJSON(next, {
        headers: {
          Authorization: `Token ${process.env.PRETIX_AUTH_TOKEN}`,
        },
      });
    }
    next = res.next;
    items = items.concat(res.results);
  }
  console.debug(
    `${DRY_PRETIX ? "[DRY] " : ""}${items.length} orders fetched.`
  );
  if (process.env.PRETIX_ORDER_FILTER) {
    const orderIds = new Set(process.env.PRETIX_ORDER_FILTER.split(","));
    console.debug("Filtering orders:", [...orderIds]);
    items = items.filter((it) => orderIds.has(it.code));
    console.debug(
      `${DRY_PRETIX ? "[DRY] " : ""}${items.length} orders remaining.`
    );
  }
  return items;
}

/**
 * @param {OrderPositionDTO}
 * @returns {boolean}
 */
function isValidTicket({ canceled, item }) {
  return !canceled && ITEM_IDS.includes(item);
}

/**
 * @param {OrderPositionDTO}
 * @returns {any}
 */
function positionToAnswers({ answers = [] }) {
  return Object.fromEntries(
    answers.map(({ question, answer }) => [question, answer])
  );
}
