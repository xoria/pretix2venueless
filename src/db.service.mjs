import pg from "pg";
import { DRY_VENUELESS } from "./dry.service.mjs";

// uses environment variables for connection details by default
const pool = new pg.Pool();
pool.on("error", console.error);

/**
 * @param {string} shortToken
 * @param {string} longToken
 * @returns {Promise<void>}
 */
export async function writeToken(shortToken, longToken) {
  if (DRY_VENUELESS) {
    return;
  }
  await pool.query(
    "INSERT INTO core_shorttoken(world_id, expires, short_token, long_token) VALUES($1, $2, $3, $4)",
    [
      process.env.VENUELESS_WORLD_ID,
      new Date(Date.now() + 3600000 * 24 * 14),
      shortToken,
      longToken,
    ]
  );
}

export async function dbClose() {
  await pool.end();
}
