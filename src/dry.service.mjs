const DRY = process.env.P2V_DRY === "1";

export const DRY_PRETIX = DRY || process.env.P2V_DRY_PRETIX === "1";
export const DRY_VENUELESS = DRY || process.env.P2V_DRY_VENUELESS === "1";
export const DRY_SMTP = DRY || process.env.P2V_DRY_SMTP === "1";
