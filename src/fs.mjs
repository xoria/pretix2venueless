import { readFile } from "node:fs/promises";

/**
 * @param {string} file
 * @returns {Promise<any>}
 */
export async function readJSON(file) {
  return JSON.parse((await readFile(file)).toString());
}
